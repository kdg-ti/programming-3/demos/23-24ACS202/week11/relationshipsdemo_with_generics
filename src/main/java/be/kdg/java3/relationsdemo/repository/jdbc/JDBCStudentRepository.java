package be.kdg.java3.relationsdemo.repository.jdbc;

import be.kdg.java3.relationsdemo.domain.Address;
import be.kdg.java3.relationsdemo.domain.Course;
import be.kdg.java3.relationsdemo.domain.School;
import be.kdg.java3.relationsdemo.domain.Student;
import be.kdg.java3.relationsdemo.repository.StudentRepository;
import org.springframework.context.annotation.Profile;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.sql.Date;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Repository
public class JDBCStudentRepository extends JDBCRepository<Student> implements StudentRepository {
    private JDBCCourseRepository courseRepository;
    private JDBCSchoolRepository schoolRepository;

    public JDBCStudentRepository(JdbcTemplate jdbcTemplate, JDBCCourseRepository courseRepository, JDBCSchoolRepository schoolRepository) {
        super(jdbcTemplate);
        this.courseRepository = courseRepository;
        this.schoolRepository = schoolRepository;
    }

    @Override
    String getTableName() {
        return "STUDENTS";
    }

    @Override
    Map<String, Object> getParameters(Student student) {
        Map<String, Object> parameters = new HashMap<>();
        parameters.put("NAME", student.getName());
        parameters.put("LENGTH", student.getLenght());
        parameters.put("BIRTHDAY", Date.valueOf(student.getBirthday()));
        parameters.put("SCHOOL_ID", student.getSchool().getId());
        return parameters;
    }

    @Override
    Student mapEntityRow(ResultSet rs, int rowid) throws SQLException {
        return new Student(rs.getInt("ID"),
                rs.getString("NAME"),
                rs.getDouble("LENGTH"),
                rs.getDate("BIRTHDAY").toLocalDate());
    }

    //Helper method: maps the columns of the DB to the attributes of the Address
    public Address mapAddressRow(ResultSet rs, int rowid) throws SQLException {
        return new Address(
                rs.getString("STREET"),
                rs.getInt("POSTAL_CODE"),
                rs.getString("CITY"));
    }

    @Override
    public List<Student> findAll() {
        List<Student> students = super.findAll();
        //load the address, school and courses:
        students.forEach(this::loadAddressAndSchool);
        students.forEach(this::loadCourses);
        return students;
    }

    @Override
    public List<Student> findBySchool(int schoolid) {
        List<Student> students = jdbcTemplate.query("SELECT * FROM STUDENTS WHERE SCHOOL_ID = ?", this::mapEntityRow, schoolid);
        //load the address, school and courses:
        students.forEach(this::loadAddressAndSchool);
        students.forEach(this::loadCourses);
        return students;
    }

    @Override
    public List<Student> findByCourse(int courseId) {
        List<Student> students = jdbcTemplate.query("SELECT * FROM STUDENTS WHERE ID IN " +
                "(SELECT STUDENT_ID FROM STUDENTS_COURSES WHERE COURSE_ID = ?)", this::mapEntityRow, courseId);
        //load the address, school and courses:
        students.forEach(this::loadAddressAndSchool);
        students.forEach(this::loadCourses);
        return students;
    }

    @Override
    public Student findById(int id) {
        Student student = super.findById(id);
        //load the address, school and courses:
        if (student != null) {
            loadAddressAndSchool(student);
            //load the courses... --> this is eager loading!
            loadCourses(student);
        }
        return student;
    }

    @Override
    public Student create(Student student) {
        student = super.create(student);
        if (student.getAddress() != null) {
            jdbcTemplate.update("INSERT INTO ADDRESS (STREET, POSTAL_CODE, CITY, STUDENT_ID) VALUES (?, ?, ?,?)",
                    student.getAddress().getStreet(), student.getAddress().getPostalCode(), student.getAddress().getCity(), student.getId());
        }
        //school should already be created--> is "independent" entity
        return student;
    }

    @Override
    public void update(Student student) {
        super.update(student);
        if (student.getAddress() != null) {
            jdbcTemplate.update("MERGE INTO ADDRESS (STREET, POSTAL_CODE, CITY, STUDENT_ID) KEY (STUDENT_ID) VALUES (?, ?, ?, ?)",
                    student.getAddress().getStreet(), student.getAddress().getPostalCode(), student.getAddress().getCity(), student.getId());
        }
        //what about the students courses??
        //we check the list of courses and update the crosstable?
        jdbcTemplate.update("DELETE FROM STUDENTS_COURSES WHERE STUDENT_ID = ?",student.getId());
        for (Course course : student.getCourses()) {
            jdbcTemplate.update("INSERT INTO STUDENTS_COURSES VALUES ( ?,? )",student.getId(),course.getId());
        }
    }

    @Override
    @Transactional
    public void delete(int id) {
        jdbcTemplate.update("DELETE FROM ADDRESS WHERE STUDENT_ID=?", id);
        jdbcTemplate.update("DELETE FROM STUDENTS_COURSES WHERE STUDENT_ID=?", id);
        super.delete(id);
    }

    private void loadCourses(Student student) {
        List<Course> courses = jdbcTemplate.query("SELECT * FROM COURSES WHERE ID IN " +
                "(SELECT COURSE_ID FROM STUDENTS_COURSES WHERE STUDENT_ID = ?) ", courseRepository::mapEntityRow, student.getId());
        student.setCourses(courses);
    }

    private void loadAddressAndSchool(Student student) {
        try {
            Address address = jdbcTemplate.queryForObject("SELECT * FROM ADDRESS WHERE STUDENT_ID = ?", this::mapAddressRow, student.getId());
            student.setAddress(address);
        } catch (EmptyResultDataAccessException e) {
            //no address found, do nothing...
        }
        try {
            School school = jdbcTemplate.queryForObject("SELECT SCHOOLS.ID, SCHOOLS.NAME " +
                    "FROM SCHOOLS INNER JOIN STUDENTS on SCHOOLS.ID = STUDENTS.SCHOOL_ID AND STUDENTS.ID = ?", schoolRepository::mapEntityRow, student.getId());
            student.setSchool(school);
        } catch (EmptyResultDataAccessException e) {
            //no school found, do nothing...
        }
    }
}
