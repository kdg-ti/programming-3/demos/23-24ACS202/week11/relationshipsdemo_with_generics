package be.kdg.java3.relationsdemo.repository.jdbc;

import be.kdg.java3.relationsdemo.domain.Entity;
import be.kdg.java3.relationsdemo.repository.EntityRepository;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.simple.SimpleJdbcInsert;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public abstract class JDBCRepository<T extends Entity> implements EntityRepository<T> {
    protected JdbcTemplate jdbcTemplate;
    protected SimpleJdbcInsert inserter;

    public JDBCRepository(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
        this.inserter = new SimpleJdbcInsert(jdbcTemplate)
                .withTableName(getTableName())
                .usingGeneratedKeyColumns("ID");
    }

    abstract String getTableName();
    abstract Map<String, Object> getParameters(T entity);
    abstract T mapEntityRow(ResultSet rs, int rowid) throws SQLException;

    @Override
    public List<T> findAll() {
        return jdbcTemplate.query("SELECT * FROM " + getTableName(), this::mapEntityRow);
    }
    @Override
    public T findById(int id) {
        return jdbcTemplate.queryForObject("SELECT * FROM " + getTableName() + " WHERE ID = ?", this::mapEntityRow, id);
    }
    @Override
    public T create(T entity) {
        entity.setId(inserter.executeAndReturnKey(getParameters(entity)).intValue());
        return entity;
    }
    @Override
    public void update(T entity) {
        StringBuilder updateString = new StringBuilder("UPDATE " + getTableName() + " SET ");
        List<Object> values = new ArrayList<>();
        getParameters(entity).forEach((key, value) -> {
            updateString.append(key).append("=?,");
            values.add(value);
        });
        values.add(entity.getId());
        updateString.deleteCharAt(updateString.length()-1);
        updateString.append(" WHERE ID = ?");
        jdbcTemplate.update(updateString.toString(), values.toArray());
    }
    @Override
    public void delete(int id) {
        jdbcTemplate.update("DELETE FROM " + getTableName() + " WHERE ID=?", id);
    }
}
