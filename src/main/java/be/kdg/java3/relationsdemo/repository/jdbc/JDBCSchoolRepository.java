package be.kdg.java3.relationsdemo.repository.jdbc;

import be.kdg.java3.relationsdemo.domain.School;
import be.kdg.java3.relationsdemo.repository.SchoolRepository;
import org.springframework.context.annotation.Profile;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

@Repository
public class JDBCSchoolRepository extends JDBCRepository<School> implements SchoolRepository {
    public JDBCSchoolRepository(JdbcTemplate jdbcTemplate) {
        super(jdbcTemplate);
    }

    @Override
    String getTableName() {
        return "SCHOOLS";
    }

    @Override
    Map<String, Object> getParameters(School school) {
        Map<String, Object> parameters = new HashMap<>();
        parameters.put("NAME", school.getName());
        return parameters;
    }

    @Override
    School mapEntityRow(ResultSet rs, int rowid) throws SQLException {
        return new School(rs.getInt("ID"),
                rs.getString("NAME"));
    }

    @Override
    public void delete(int id) {
        jdbcTemplate.update("DELETE FROM ADDRESS WHERE STUDENT_ID IN (SELECT STUDENT_ID FROM STUDENTS WHERE SCHOOL_ID=?)", id);
        jdbcTemplate.update("DELETE FROM STUDENTS_COURSES WHERE STUDENT_ID IN (SELECT STUDENT_ID FROM STUDENTS WHERE SCHOOL_ID=?)", id);
        jdbcTemplate.update("DELETE FROM STUDENTS WHERE SCHOOL_ID=?", id);
        super.delete(id);
    }
}
