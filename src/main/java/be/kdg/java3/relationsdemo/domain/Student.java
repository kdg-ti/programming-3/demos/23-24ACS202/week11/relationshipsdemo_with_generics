package be.kdg.java3.relationsdemo.domain;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

public class Student extends AbstractEntity{
    private String name;
    private double lenght;
    private LocalDate birthday;

    //relation: One to One
    private Address address;

    //Relation Many to One
    private School school;

    //Many-To-Many relation
    private List<Course> courses = new ArrayList<>();

    public Student(int id, String name, double lenght, LocalDate birthday) {
        super(id);
        this.name = name;
        this.lenght = lenght;
        this.birthday = birthday;
    }

    public Student(String name, double lenght, LocalDate birthday) {
        this.name = name;
        this.lenght = lenght;
        this.birthday = birthday;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getLenght() {
        return lenght;
    }

    public void setLenght(double lenght) {
        this.lenght = lenght;
    }

    public LocalDate getBirthday() {
        return birthday;
    }

    public void setBirthday(LocalDate birthday) {
        this.birthday = birthday;
    }

    public Address getAddress() {
        return address;
    }

    public void setAddress(Address address) {
        this.address = address;
        address.setStudent(this);
    }

    public School getSchool() {
        return school;
    }

    public void setSchool(School school) {
        this.school = school;
        school.addStudent(this);
    }

    public void addCourse(Course course){
        this.courses.add(course);
        course.addStudent(this);
    }

    public List<Course> getCourses() {
        return courses;
    }

    public void setCourses(List<Course> courses) {
        this.courses = courses;
        //should we for each course set the student?
        //this list of students in the course object will depend on wether the student was loaded from db
        //maybe better not to load?
        //courses.forEach(c->c.addStudent(this));
    }

    @Override
    public String toString() {
        return "Student{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", lenght=" + lenght +
                ", birthday=" + birthday +
                '}' + (address!=null?"Address: " + address.toString():"<no address>")+
               ", " + (school!=null?"School: " + school.toString():"<no school>");
    }
}
