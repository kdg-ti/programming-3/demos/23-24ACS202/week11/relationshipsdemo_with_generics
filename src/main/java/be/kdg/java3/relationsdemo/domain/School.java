package be.kdg.java3.relationsdemo.domain;

import java.util.ArrayList;
import java.util.List;

public class School extends AbstractEntity{
    private String name;

    //One-to-Many relationship
    //But do we need this attribute?
    //If we wanna be able to save a school and as a consequence
    //update all the students
    private List<Student> students = new ArrayList<>();

    public School(String name) {
        this.name = name;
    }

    public School(int id, String name) {
        super(id);
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void addStudent(Student s){
        students.add(s);
    }

    @Override
    public String toString() {
        return "School{" +
                "id=" + id +
                ", name='" + name + '\'' +
                '}';
    }
}
