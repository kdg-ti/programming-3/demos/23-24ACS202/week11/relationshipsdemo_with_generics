package be.kdg.java3.relationsdemo.domain;

import java.util.ArrayList;
import java.util.List;

public class Course extends AbstractEntity{
    private String name;
    private int academicYear;

    //Many-to-many relation
    private List<Student> students = new ArrayList<>();

    public Course(String name, int academicYear) {
        this.name = name;
        this.academicYear = academicYear;
    }

    public Course(int id, String name, int academicYear) {
        super(id);
        this.name = name;
        this.academicYear = academicYear;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAcademicYear() {
        return academicYear;
    }

    public void setAcademicYear(int academicYear) {
        this.academicYear = academicYear;
    }

    public void addStudent(Student student){
        students.add(student);
    }

    public List<Student> getStudents() {
        return students;
    }

    public void setStudents(List<Student> students) {
        this.students = students;
    }

    @Override
    public String toString() {
        return "Course{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", academicYear=" + academicYear +
                '}';
    }
}
