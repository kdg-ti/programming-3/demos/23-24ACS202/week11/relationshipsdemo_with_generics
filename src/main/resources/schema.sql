drop table if exists SCHOOLS;
create table SCHOOLS
(
    ID   INTEGER auto_increment
        primary key
        unique,
    NAME CHARACTER VARYING(100) not null
);
DROP TABLE IF EXISTS ADDRESS;
DROP TABLE IF EXISTS STUDENTS;
create table STUDENTS
(
    ID       INTEGER auto_increment
        primary key
        unique,
    NAME     CHARACTER VARYING(100),
    LENGTH   DOUBLE PRECISION,
    BIRTHDAY DATE,
    SCHOOL_ID INTEGER not null,
    constraint FK_SCHOOL_ID
        foreign key (SCHOOL_ID) references SCHOOLS

);
create table ADDRESS
(
    STREET      CHARACTER VARYING(100) not null,
    POSTAL_CODE INTEGER                not null,
    CITY        CHARACTER VARYING(100) not null,
    STUDENT_ID  INTEGER                not null,
    constraint FOREIGN_KEY_NAME
        foreign key (STUDENT_ID) references STUDENTS
);
DROP TABLE IF EXISTS COURSES;
create table COURSES
(
    ID            INTEGER auto_increment
        primary key
        unique,
    NAME          CHARACTER VARYING(100) not null,
    ACADEMIC_YEAR INTEGER                not null
);
DROP TABLE IF EXISTS STUDENTS_COURSES;
create table STUDENTS_COURSES
(
    STUDENT_ID INTEGER not null,
    COURSE_ID  INTEGER not null,
    constraint FK_COURSE_ID
        foreign key (COURSE_ID) references COURSES,
    constraint FK_STUDENT_ID
        foreign key (STUDENT_ID) references STUDENTS
);
